#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <iostream>
#include <list>
#include <string>
#include "uart.h"
#include "helpers.h"

using std::cout;
using std::endl;

#include "froly-common/mavlink/froly/mavlink.h"
static mavlink_message_t message;
static mavlink_status_t status;

static bool its_sender_time = true;
static int tx_seq = -1;
static int rx_seq = -2;
static int tx = 0;
static int rx = 0;

pct::Uart::Uart(Params p, size_t port_rate, std::list<Packet> sequence) :
    p_(p),
    _port_rate(port_rate),
    _sequence(sequence) {
    open_port();
    _it = _sequence.begin();
    if (!p_.delay.empty()) {
        delay_ = std::stoi(p_.delay);
    }
    if (!p_.verbose.empty()) {
        verbose_ = std::stoi(p_.verbose);
    }
}

bool pct::Uart::open_port() {
    _serial_fd = open(p_.port.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
    if (_serial_fd == -1) {
        printf("### open %s error\n", p_.port.c_str());
        exit(EXIT_FAILURE);
    }

    struct termios att;

    if (tcgetattr(_serial_fd, &att) < 0) {
        exit(EXIT_FAILURE);
    }

    att.c_iflag = 0;
    att.c_oflag = 0;
    att.c_lflag = 0;
    att.c_cflag = 0;

    att.c_cflag = CS8 | CREAD | _port_rate;
    att.c_cc[VMIN] = 5;
    att.c_cc[VTIME] = 2;

    if (cfsetispeed(&att, _port_rate) != 0) {
        exit(EXIT_FAILURE);
    }

    if (tcsetattr(_serial_fd, TCSANOW, &att) != 0) {
        exit(EXIT_FAILURE);
    }

    printf("UART %s is open\n", p_.port.c_str());

    return true;
}

void pct::Uart::cmd(std::string cmd) {
    std::vector<uint8_t> data;
    std::copy(cmd.begin(), cmd.end(), std::back_inserter(data));
    data.push_back(0x0D);

    for (auto i : data) {
        write(_serial_fd, &i, 1);
    }
    read_detached();
}

void pct::Uart::read_detached() {
    fd_set fds;
    struct timeval tv;
    FD_ZERO(&fds);
    FD_SET(_serial_fd, &fds);
    /* timeout */
    tv.tv_sec = 1;
    tv.tv_usec = 0;
    int r = select(_serial_fd + 1, &fds, NULL, NULL, &tv);
    if (r > 0) {
        int res = read(_serial_fd, _buffer, 256);
        if (res >= 0) {
            for (auto i = 0; i < res; ++i) {
                printf("%c", _buffer[i]);
            }
            printf("\n");
        }
    }
}

void pct::Uart::tick() {
    auto verbose = 0;
    if (its_sender_time) {
        its_sender_time = false;
        usleep(delay_);

        if (tx_seq != rx_seq && tx_seq > 0) {
            printf("# ");
            std::cout << std::flush;
        }

        if (_it != _sequence.end() && tx_seq == rx_seq) {
            ++_it;
        }

        if (_it == _sequence.end()) {
            printf("--------------------------------------------------------------\n");
            cout << "\nSEQUENCE EMPTY" << endl;
            exit(0);
        }

        auto count = 0;
        auto packet = *_it;
        for (auto value : packet) {
            uint8_t v = value;
            count += write(_serial_fd, &v, 1);
            usleep(1);
            std::cout << std::flush;
            if (verbose_ > 1) {
                printf("%02X ", *(&v));
            }
        }
        if (verbose_ > 1) {
            printf("\n");
        }
        if (verbose_ > 0) {
            printf("\n-> seq='%d'\n\n", packet[4]);
        }
        tx++;
        tx_seq = packet[4];
    }

    fd_set fds;
    struct timeval tv;

    FD_ZERO(&fds);
    FD_SET(_serial_fd, &fds);

    /* timeout */
    tv.tv_sec = 0;
    tv.tv_usec = delay_ * 2 + 50000;

    if (rx_seq != tx_seq) {
        int r = select(_serial_fd + 1, &fds, NULL, NULL, &tv);
        if (r > 0) {
            int res = read(_serial_fd, _buffer, 256);
            if (res >= 0) {
                for (auto i = 0; i < res; ++i) {
                    if (verbose_ > 1) {
                        printf("%02X ", _buffer[i]);
                    }
                    wrapper(_buffer[i]);
                }
                if (verbose_ > 1) {
                    printf("\n");
                }
            }
        } else {
            its_sender_time = true;
        }
    } else {
        usleep(1000);
        its_sender_time = true;
    }
}

void pct::Uart::wrapper(uint8_t b) {
    if (mavlink_parse_char(MAVLINK_COMM_0, b, &message, &status)) {
        if (verbose_ > 1) {
            printf("\n");
        }
        rx++;
        if (message.msgid != MAVLINK_MSG_ID_ACK) {
            rx_seq = message.seq;
            its_sender_time = true;
        }
        if (verbose_ > 0 && message.msgid != MAVLINK_MSG_ID_ACK) {
            printf("\n<- seq='%d' received='%d' sent='%d' parse_error='%d' buffer_overrun='%d' packet_rx_drop='%d'\n\n",
                message.seq,
                status.msg_received,
                status.packet_rx_success_count,
                status.parse_error,
                status.buffer_overrun,
                status.packet_rx_drop_count);
        }

        switch (message.msgid) {
            case MAVLINK_MSG_ID_HEARTBEAT : {
                char name[5];
                printf("\nHEARTBEAT  | pc   | stm  | pc   | stm  |\n");
                printf("--------------------------------------------------------------\n");
                printf("HEARTBEAT  | tx   | rx   | rx   | tx   | remote_crc | local_crc\n");
                printf("--------------------------------------------------------------\n");
                printf("HEARTBEAT  | %03d  | %03d  | %03d  | %03d  |  %05d     | %05d\n",
                tx,
                mavlink_msg_heartbeat_get_radio_rx(&message),
                rx,
                mavlink_msg_heartbeat_get_radio_tx(&message),
                mavlink_msg_heartbeat_get_route_crc(&message),
                pct::get_route_crc());

                printf("armed %d, hdop %d, connected %d, voltage_rel %d, fix %d tx:ack [%d:%d]\n",
                     mavlink_msg_heartbeat_get_armed(&message),
                     mavlink_msg_heartbeat_get_gnss_hdop(&message),
                     mavlink_msg_heartbeat_get_connected(&message),
                     mavlink_msg_heartbeat_get_voltage_relative(&message),
                     mavlink_msg_heartbeat_get_gnss_fix(&message),
                     mavlink_msg_heartbeat_get_radio_tx(&message),
                     mavlink_msg_heartbeat_get_radio_tx_ack(&message));

                mavlink_msg_heartbeat_get_system_name(&message, name);
                name[4] = '\0';
                printf("name %s\n", name);
            }
        break;
            case MAVLINK_MSG_ID_PARAM_VALUE : {
                char name[17];
                mavlink_msg_param_value_get_param_id(&message, name);
                name[16] = '\0';
                int32_t value = mavlink_msg_param_value_get_param_value(&message);
                printf("param '%s' : '%d'\n", name, value);
            }
        break;
            case MAVLINK_MSG_ID_TELEMETRY : {

            }
        break;
            case MAVLINK_MSG_ID_ACK : {
                if (verbose_ > 0) {
                    printf("%d ", mavlink_msg_ack_get_processing_time(&message));
                    std::cout << std::flush;
                }
            }
        break;
            case MAVLINK_MSG_ID_RADIO_TELEMETRY : {
                printf("rssi %0.2f snr %0.2f rssi %0.2f snr %0.2f\n",
                    (double) mavlink_msg_radio_telemetry_get_rssi_local(&message),
                    (double) mavlink_msg_radio_telemetry_get_snr_local(&message),
                    (double) mavlink_msg_radio_telemetry_get_rssi_remote(&message),
                    (double) mavlink_msg_radio_telemetry_get_snr_remote(&message));
            }
        break;
        }
    }
}
