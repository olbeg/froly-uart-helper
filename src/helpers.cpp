#include "helpers.h"
#include <memory.h>
#include <inttypes.h>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <functional>
#include <sstream>
#include <iomanip>
#include <chrono>
#include "froly-common/mavlink/froly/mavlink.h"
#include "froly-common/include/froly-protocol.h"

#include <random>

using std::cout;
using std::endl;

#define BUFFER_LENGTH 2041
static uint8_t buf[BUFFER_LENGTH];
static uint16_t calc_route_crc(uint16_t crc);
uint16_t crc_ = 0;

uint16_t pct::get_route_crc() {
    return crc_;
}

Packet pct::get_packet(u8 number, Packet payload) {
    Packet packet = { 0xAA, number, static_cast<u8>(payload.size()) };
    std::copy(payload.begin(), payload.end(), std::back_inserter(packet));
    u8 sum = std::accumulate(packet.begin(), packet.end(), 0, std::plus<int>());
    packet.push_back(sum);
    packet.push_back((u8)(sum + sum));
    packet.push_back(0xDD);
    packet.push_back(0xEE);
    packet.push_back('\r');
    return packet;
}

u8 pct::get_number() {
    static u8 number = 0;
    if (++number == 0)
        number = 1;
    return number;
}

uint16_t calc_route_crc(uint16_t crc, struct s_waypoint wp) {
    // crc += crc16((const uint8_t*)&wp, sizeof(struct s_waypoint));
    crc += wp.latitude / 10.0;
    crc += wp.longitude / 10.0;
    crc += wp.altitude;
    crc += wp.delay;
    return crc;
}

std::list<Packet> pct::string_to_packet(std::string line, std::string sysid) {
    std::list<Packet> sequence;
    size_t sz;
    auto id = (sysid.empty() ? 2 : std::stoi(sysid));

    auto pos = line.find("HEARTBEAT");
    if (pos != std::string::npos) {
        uint32_t count = std::stoi(line.substr(strlen("HEARTBEAT") + 1, line.length()), &sz);
        for (size_t i = 0; i < count; ++i) {
            mavlink_status_t* status = mavlink_get_channel_status(MAVLINK_COMM_0);
            std::minstd_rand simple_rand;
            // Use simple_rand.seed() instead of srand():
            simple_rand.seed(i);
            // status->current_tx_seq = 77;
            // status->current_tx_seq = simple_rand();

            mavlink_message_t msg;
            mavlink_control_t command;
            command.responce_required = FROLY_RESPONCE_REQUIRE; //(status->current_tx_seq % 4 != 0 ? FROLY_RESPONCE_REQUIRE : FROLY_RESPONCE_OMIT);
            mavlink_msg_control_encode(1, id, &msg, &command);
            uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
            std::vector<uint8_t> packet;
            for (auto i = 0; i < len; ++i) {
                packet.push_back(buf[i]);
            }
            sequence.push_back(packet);
        }
    }
    pos = line.find("DUMMY");
    if (pos != std::string::npos) {
        uint32_t count = std::stoi(line.substr(strlen("DUMMY") + 1, line.length()), &sz);
        for (size_t i = 0; i < count; ++i) {
            mavlink_message_t msg;
            mavlink_dummy_t command;
            command.target_system = id;
            mavlink_msg_dummy_encode(1, id, &msg, &command);
            uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
            std::vector<uint8_t> packet;
            for (auto i = 0; i < len; ++i) {
                packet.push_back(buf[i]);
            }
            sequence.push_back(packet);
        }
    }
    pos = line.find("POINT");
    if (pos != std::string::npos) {
        static int seq = 1;
        mavlink_status_t* status = mavlink_get_channel_status(MAVLINK_COMM_0);
        std::minstd_rand simple_rand;
        // Use simple_rand.seed() instead of srand():
        simple_rand.seed(42);
        // status->current_tx_seq = 77;
        mavlink_message_t msg;
        mavlink_mission_item_t item;
        item.type = WP_TYPE_NAV;
        item.x = 55.01 * 1E7 + seq++ * 10;
        item.y = 37.01 * 1E7;
        item.z = 100;
        item.delay = 1;

        struct s_waypoint wp = {
            .mask = 0,
            .type = 0,
            .latitude = (double)item.x,
            .longitude = (double)item.y,
            .altitude = item.z,
            .yaw = 0,
            .delay = item.delay
        };
        crc_ = calc_route_crc(crc_, wp);

        mavlink_msg_mission_item_encode(1, id, &msg, &item);
        uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
        std::vector<uint8_t> packet;
        for (auto i = 0; i < len; ++i) {
            packet.push_back(buf[i]);
        }
        sequence.push_back(packet);
    }
    auto cmd = -1;
    auto param1 = -1;
    auto param2 = 0;
    auto param3 = -1;
    pos = line.find("UPLOAD");
    if (pos != std::string::npos) {
        cmd = FROLY_CMD_UPLOAD_MISSION;
    }
    pos = line.find("FASTENER_LOCK");
    if (pos != std::string::npos) {
        cmd = FROLY_CMD_FASTENER;
        param1 = FROLY_FASTENER_LOCK;
        param2 = std::stoi(line.substr(strlen("FASTENER_LOCK") + 1, line.length()), &sz);
        std::cout << "param2 " << param2 << std::endl;
    }
    pos = line.find("FASTENER_UNLOCK");
    if (pos != std::string::npos) {
        cmd = FROLY_CMD_FASTENER;
        param1 = FROLY_FASTENER_UNLOCK;
    }
    pos = line.find("VIDEO");
    if (pos != std::string::npos) {
        cmd = FROLY_CMD_VIDEO_SOURCE;
        param1 = std::stoi(line.substr(strlen("VIDEO") + 1, line.length()), &sz);
        std::cout << "param1 " << param2 << std::endl;
    }
    pos = line.find("SET_NAME");
    if (pos != std::string::npos) {
        cmd = FROLY_CMD_WRITE_NAME;
        std::string name = line.substr(strlen("SET_NAME") + 1, line.length());
        uint32_t code = 0;
        code |= name.c_str()[0];
        code |= name.c_str()[1] << 8;
        code |= name.c_str()[2] << 16;
        code |= name.c_str()[3] << 24;
        cout << "code " << code << endl;
        param3 = code;
        char foo[5] = { code & 0xFF, (code & 0xFF00) >> 8, (code & 0xFF0000) >> 16, (code & 0xFF000000) >> 24, '\0' };
        cout << "name " << foo << endl;
    }
    pos = line.find("SET_ID");
    if (pos != std::string::npos) {
        cmd = FROLY_CMD_WRITE_ID;
        param3 = std::stoi(line.substr(strlen("SET_ID") + 1, line.length()), &sz);
    }
    pos = line.find("SET_FIRMWARE");
    if (pos != std::string::npos) {
        cmd = FROLY_CMD_WRITE_FIRMWARE;
        param3 = std::stoi(line.substr(strlen("SET_FIRMWARE") + 1, line.length()), &sz);
    }
    pos = line.find("GET_PARAM");
    if (pos != std::string::npos) {
        cmd = FROLY_CMD_GET_PARAM;
        param1 = std::stoi(line.substr(strlen("GET_PARAM") + 1, line.length()), &sz);
    }
    pos = line.find("GET_RSSI");
    if (pos != std::string::npos) {
        cmd = FROLY_CMD_GET_RADIO_TELEMETRY;
        param1 = FROLY_TELEMETRY_SOURCE_LOCAL;
    }
    pos = line.find("SET_PARAM");
    if (pos != std::string::npos) {
        cmd = FROLY_CMD_SET_PARAM;
        auto space = line.find(' ', pos + strlen("SET_PARAM") + 2);
        param1 = std::stoi(line.substr(strlen("SET_PARAM") + 1, space), &sz);
        param2 = std::stoi(line.substr(space, line.length()), &sz);
    }
    pos = line.find("MODULATION");
    if (pos != std::string::npos) {
        cmd = FROLY_CMD_SET_MODULATION;
        auto space = line.find(' ', pos + strlen("MODULATION") + 2);
        param1 = std::stoi(line.substr(strlen("MODULATION") + 1, space), &sz);
        param2 = std::stoi(line.substr(space, line.length()), &sz);
        cout << param1 << " " << param2 << endl;
    }
    if (cmd != -1) {
        mavlink_status_t* status = mavlink_get_channel_status(MAVLINK_COMM_0);
        std::minstd_rand simple_rand;
        // Use simple_rand.seed() instead of srand():
        simple_rand.seed(43);
        // status->current_tx_seq = simple_rand();
        mavlink_message_t msg;
        mavlink_command_t command;
        command.cmd = cmd;
        command.param1 = param1;
        command.param2 = param2;
        command.param3 = param3;
        mavlink_msg_command_encode(1, id, &msg, &command);
        uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
        std::vector<uint8_t> packet;
        for (auto i = 0; i < len; ++i) {
            packet.push_back(buf[i]);
        }
        sequence.push_back(packet);
    }
    pos = line.find("CLEAR");
    if (pos != std::string::npos) {
        mavlink_status_t* status = mavlink_get_channel_status(MAVLINK_COMM_0);
        std::minstd_rand simple_rand;
        // Use simple_rand.seed() instead of srand():
        simple_rand.seed(43);
        // status->current_tx_seq = 77;
        // status->current_tx_seq = simple_rand();
        mavlink_message_t msg;
        mavlink_command_t command;
        command.cmd = FROLY_CMD_CLEAR_MISSION;
        mavlink_msg_command_encode(1, id, &msg, &command);
        uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
        std::vector<uint8_t> packet;
        for (auto i = 0; i < len; ++i) {
            packet.push_back(buf[i]);
        }
        sequence.push_back(packet);
        crc_ = 0;
    }
    pos = line.find("RESET_STAT");
    if (pos != std::string::npos) {
        mavlink_status_t* status = mavlink_get_channel_status(MAVLINK_COMM_0);
        std::minstd_rand simple_rand;
        // Use simple_rand.seed() instead of srand():
        simple_rand.seed(43);
        // status->current_tx_seq = 77;
        // status->current_tx_seq = simple_rand();
        mavlink_message_t msg;
        mavlink_command_t command;
        command.cmd = FROLY_CMD_CLEAR_STAT;
        mavlink_msg_command_encode(1, id, &msg, &command);
        uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
        std::vector<uint8_t> packet;
        for (auto i = 0; i < len; ++i) {
            packet.push_back(buf[i]);
        }
        sequence.push_back(packet);
    }
    return sequence;
}

void pct::get_msec(int64_t start_msec, char **filename) {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    int64_t msecs = (int64_t)(tv.tv_sec) * 1000 + (int64_t)(tv.tv_usec) / 1000;
    msecs -= start_msec;

    const int n = snprintf(NULL, 0, "%" PRId64, msecs);
    char buffer[n + 1];
    int c = snprintf(buffer, n + 1, "%" PRId64, msecs);

    (*filename) = reinterpret_cast<char*>(malloc(n));
    strcpy((*filename), buffer);
}

#if (PCT_DEBUG == 1)
    int main(int argc, char** argv) {
        std::ifstream in(argv[1]);
        for (std::string line; std::getline(in, line); )
            string_to_packet(line);
        return 0;
    }
#endif

uint8_t pct::ascii_hex_to_byte(std::string str) {
    if (str.length() != 2) {
        return 0;
    }
    uint8_t value = stoi(str, 0, 16);
    return value;
}

static mavlink_message_t message;
static mavlink_status_t status;
static int8_t mode_ = -2;
static bool start_ = false;
void pct::parse(std::string line, bool kml) {
    for (size_t i = 0; i < line.length() - 1; i += 2) {
        auto byte = pct::ascii_hex_to_byte(line.substr(i, 2));
        if (mavlink_parse_char(MAVLINK_COMM_0, byte, &message, &status)) {
            switch(message.msgid) {
                case MAVLINK_MSG_ID_HEARTBEAT : {
                    if (kml == false) {
                        printf("voltage %d rel %d%% gnss_sat %d gnss_fix %d mode %d \n",
                            mavlink_msg_heartbeat_get_voltage(&message),
                            mavlink_msg_heartbeat_get_voltage_relative(&message),
                            mavlink_msg_heartbeat_get_satellites_count(&message),
                            mavlink_msg_heartbeat_get_gnss_fix(&message),
                            mavlink_msg_heartbeat_get_mode(&message)
                            );
                    } else {
                        auto mode = mavlink_msg_heartbeat_get_mode(&message);
                        if (mode_ != mode) {
                            if (mode_ != -2) {
                                cout << kml::track_footer() << endl;
                            }
                            cout << "<!--" << (double) mode << "-->" << endl;
                            cout << kml::track_header(mode) << endl;
                            mode_ = mode;
                            start_ = true;
                        }
                    }
                }
            break;
                case MAVLINK_MSG_ID_TELEMETRY : {
                    if (kml == false) {
                    printf("lat %0.7f lon %0.7f alt %d roll %d pitch %d vel %0.1f\n",
                        mavlink_msg_telemetry_get_latitude(&message) / 10000000.,
                        mavlink_msg_telemetry_get_longitude(&message) / 10000000.,
                        mavlink_msg_telemetry_get_altitude(&message),
                        mavlink_msg_telemetry_get_roll(&message),
                        mavlink_msg_telemetry_get_pitch(&message),
                        mavlink_msg_telemetry_get_vel_h(&message) / 10.);
                    } else {
                        auto altitude = mavlink_msg_telemetry_get_altitude(&message) + 200;
                        auto latitude = mavlink_msg_telemetry_get_latitude(&message) / 10000000.;
                        auto longitude = mavlink_msg_telemetry_get_longitude(&message) / 10000000.;
                        if (start_) {
                            cout << kml::when() << endl;
                            cout << kml::where(latitude, longitude, altitude) << endl;
                        }
                    }
                }
            break;
            }
        }
    }
}

std::string kml::header() {
    return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><kml xmlns=\"http://www.opengis.net/kml/2.2\" xmlns:gx=\"http://www.google.com/kml/ext/2.2\"><Document><name>Track</name><description></description><Style id=\"red\"><LineStyle><color>7f00ffff</color><width>4</width></LineStyle></Style><Style id=\"blue\"><LineStyle><color>7fff0000</color><width>4</width></LineStyle></Style><Style id=\"red\"><LineStyle><color>7f0000ff</color><width>4</width></LineStyle></Style>";
}

std::string kml::footer() {
    return "</Document></kml>";
}

std::string kml::where(double lat, double lon, double alt) {
    std::stringstream stream;
    stream << "<gx:coord>" << std::setprecision(15) << lon << " " << lat << " " << std::setprecision(5) << alt << "</gx:coord>";
    return stream.str();
}

// NOTE: fix timing
std::string kml::when() {
    static auto seconds = 0;
    auto foo = std::chrono::system_clock::now();
    foo += std::chrono::seconds(seconds++);
    auto bar = std::chrono::system_clock::to_time_t(foo);
    char mbstr[100];
    std::strftime(mbstr, sizeof(mbstr), "%H:%M:%S.001Z", std::localtime(&bar));
    std::stringstream stream;
    stream << "<when>2017-04-07T" << mbstr << "</when>";
    return stream.str();
}

std::string kml::track_header(int8_t mode) {
    std::stringstream stream;
    double m = mode;
    stream << "<Placemark><name>Track " << std::setprecision(2) << m << "</name><description></description><styleUrl>#red</styleUrl><gx:Track id=\"Track\"><altitudeMode>absolute</altitudeMode><gx:interpolate>0</gx:interpolate>";
    return stream.str();
}

std::string kml::track_footer() {
    return "</gx:Track></Placemark>";
}
