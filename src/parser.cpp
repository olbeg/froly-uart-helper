#include <iostream>
#include <algorithm>
#include "parser.h"

using std::cout;
using std::endl;

pct::Parser::Parser() {

}

void pct::Parser::append(u8 *data, u8 size) {
    for (auto i = 0; i < size; ++i) {
        _buffer.push_back(data[i]);
    }
}

bool pct::Parser::process() {
    bool found = false;
    u8 byte = 0;

    while (_buffer.size() != 0) {
        auto byte = _buffer.front();
        if (byte != 0xBB) {
            _buffer.pop_front();
        } else {
            break;
        }
    }

    size_t offset = 5;
    if (_buffer.size() > 9) {
        size_t size = _buffer[2];
        if (_buffer.size() >= size) {
            size_t dd_candidate = _buffer[size + offset];
            if (_buffer[size + offset] == 0xEE) {
                u8 sum = 0;
                for (size_t i = 0; i < size + offset - 2; ++i) {
                    sum += _buffer[i];
                }
                if (sum == _buffer[size + offset - 2]) {
                    found = true;
                    Packet payload;
                    std::copy(_buffer.begin() + 3, _buffer.begin() + 3 + size, std::back_inserter(payload));
                    cout << "\n\npayload:\n";
                    for (size_t j = 0; j < payload.size(); ++j) {
                        printf("%02X ", payload.at(j));
                    }
                    cout << endl;
                }
                _buffer.erase(_buffer.begin(), _buffer.begin() + size + offset);
            }
        }
    }

    cout << "deque size: " << _buffer.size() << endl;
    return found;
}

std::set<Packet> pct::Parser::tokenize(Packet payload) {
    return {};
}

