#include <getopt.h>
#include <termios.h>
#include <fstream>
#include <iostream>
#include "helpers.h"
#include "uart.h"

#include <random>
#include <chrono>

#include "froly-common/mavlink/froly/mavlink.h"

void usage(FILE *fp, int argc, char **argv);
Params get_params(int argc, char** argv);

void usage(FILE *fp, int argc, char **argv) {
    fprintf(fp,
            "Usage: %s [options]\n"
            "Version 0.3\n"
            "Options:\n"
            "<-t>  Terminal type (0 - mavlink, 1 - modem)\n"
            "<-u>  Serial port name\n"
            "[-d]  Delay in between packets\n"
            "[-f]  Path to script\n"
            "[-h]  Print this message\n"
            "[-s]  Sysid\n"
            "[-v]  Verbosity level (0..2)\n"
            "",
            argv[0]);
}

static const struct option
long_options[] = {
    { "uart", required_argument, NULL, 'u' },
    { "file", optional_argument, NULL, 'f' },
    { "type", optional_argument, NULL, 't' },
    { "delay", optional_argument, NULL, 'd' },
    { "sysid", optional_argument, NULL, 's' },
    { "verbose", optional_argument, NULL, 'v' },
    { "help", no_argument, NULL, 'h' }
};

Params get_params(int argc, char** argv) {
    Params input;

    struct timeval tv;
    gettimeofday(&tv, NULL);

    // check && set datetime
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    if (tm.tm_year + 1900 < 2015) {
        system("date -s \"2015-08-12\"");
    }

    if (argc < 2) {
        usage(stdout, argc, argv);
        exit(-1);
    }

    while (true) {
        char *optstring = (char*)"";
        int idx;
        int c = getopt_long_only(argc, argv, optstring, long_options, &idx);
        if (c == -1)
            break;

        switch (c) {
            case 'u' :
                //printf("uart %s\n", optarg);
                input.port = optarg;
        break;
            case 'f' :
                //printf("file %s\n", optarg);
                input.file = optarg;
        break;
            case 't' :
                //printf("type %s\n", optarg);
                input.type = optarg;
        break;
            case 'd' :
                //printf("delay %s\n", optarg);
                input.delay = optarg;
        break;
            case 'v' :
                //printf("verbose %s\n", optarg);
                input.verbose = optarg;
        break;
            case 's' :
                //printf("sysid %s\n", optarg);
                input.sysid = optarg;
        break;
            case 'h' :
                usage(stdout, argc, argv);
                exit(0);
            default:
                usage(stdout, argc, argv);
                exit(-1);
        }
    }

    return input;
}

int main(int argc, char **argv) {
    Params input = get_params(argc, argv);
    std::ifstream in(input.file);

    if (input.type.compare("1") == 0) {
        for (std::string line; getline(in, line); ) {
            pct::parse(line, false);
        }
        exit(0);
    }

    if (input.type.compare("2") == 0) {
        std::cout << kml::header() << std::endl;
        for (std::string line; getline(in, line); ) {
            pct::parse(line, true);
        }
        std::cout << kml::track_footer() << std::endl;
        std::cout << kml::footer() << std::endl;
        exit(0);
    }

    std::list<Packet> generated;
    for (std::string line; getline(in, line); ) {
        std::list<Packet> tmp = pct::string_to_packet(line, input.sysid);
        std::copy(begin(tmp), end(tmp), std::back_inserter(generated));
    }
    pct::Uart uart(input, B115200, generated);

    auto start = std::chrono::system_clock::now();
    auto end = std::chrono::system_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << std::endl;
    printf("\nHEARTBEAT  | pc   | stm  | pc   | stm  |\n");
    printf("--------------------------------------------------------------\n");
    printf("HEARTBEAT  | tx   | rx   | rx   | tx   | remote_crc | local_crc\n");
    while (true) {
        if (input.type.compare("0") == 0) {
            uart.tick();
        } else {
            std::cout << "+";
            std::string str;
            getline(std::cin, str);
            if (str.compare("q") == 0) {
                exit(0);
            }
            std::string cmd = "+" + str;
            std::cout << "sending '" << cmd << "': ";
            uart.cmd(cmd);
            str.clear();
        }
    }

    return 0;
}
