#pragma once

#include <deque>
#include <set>
#include <stdint.h>

#include "helpers.h"

namespace pct {
    class Parser {
    public:
        Parser();
        void append(u8 *data, u8 size);
        bool process();
    private:
        std::deque<u8> _buffer;
        std::set<Packet> tokenize(Packet payload);
    };
}
