#pragma once

#define PCT_DEBUG 0

#include <stdint.h>
#include <list>
#include <vector>
#include <string>

#include <sys/time.h>

using u8 = uint8_t;
using u32 = uint32_t;
using Packet = std::vector<u8>;

using Params = struct input_params_s {
    std::string file;
    std::string port;
    std::string type;
    std::string delay;
    std::string verbose;
    std::string sysid;
};

namespace pct {
    u8 get_number();
    std::list<Packet> string_to_packet(std::string line, std::string sysid);
    Packet get_packet(u8 number, Packet payload);
    void get_msec(int64_t start_msec, char **filename);
    uint16_t get_route_crc();
    uint8_t ascii_hex_to_byte(std::string str);
    void parse(std::string line, bool kml);
}

namespace kml {
    std::string header();
    std::string footer();
    std::string where(double lat, double lon, double alt);
    std::string when();
    std::string track_header(int8_t mode);
    std::string track_footer();
}
