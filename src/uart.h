#pragma once

#include "helpers.h"
#include "parser.h"

namespace pct {
    class Uart {
    public:
        Uart(Params p, size_t port_rate, std::list<Packet> sequence);
        void tick();
        void cmd(std::string cmd);
        void read_detached();
    private:
        bool open_port();
        Params p_;
        int delay_ = 0;
        int verbose_ = 0;
        size_t _port_rate;
        std::list<Packet> _sequence;
        std::list<Packet>::iterator _it;
        int _serial_fd = -1;
        u8 _buffer[256] = {0};
        Parser parser;
        void wrapper(uint8_t b);
    };
}
