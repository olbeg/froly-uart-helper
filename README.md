#### Froly protocol packet generator & uart communication utility
* `$ cmake . && make`
* `$ ./uarter -u=/dev/ttyUSB0 -f=scripts/cmd.froly -t=<0 | 1>`

#### Supported packets within .froly
* `HEARTBEAT <count>` command packet, consider `FROLY_RESPONCE_REQUIRED` enabled
* `DUMMY` packet with `uint8_t` only param
* `POINT` provide `MISSION_ITEM` with auto-incremented lat/lon
* `UPLOAD` will upload mission into px4
* `CLEAR` reset mission
* `RESET_STAT` reset rx-tx statistics

